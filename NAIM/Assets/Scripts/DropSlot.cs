using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropSlot : MonoBehaviour, IDropHandler
{
    public GameObject item;
    [SerializeField] int id;
    [SerializeField] Spotify spotifi;

    void Start()
    {
        
    }

    public void OnDrop(PointerEventData eventData)
    {
        if(!item && DragAndDrop.itemDragging.GetComponent<DragAndDrop>().id == id)
        {
            item = DragAndDrop.itemDragging;
            item.transform.SetParent(transform);
            item.transform.position = transform.position;

            if (item.GetComponent<DragAndDrop>().correcto)
            {
                spotifi.Suma();
            }
        }
    }


    void Update()
    {
        if (item != null && item.transform.parent != transform &&
            item.GetComponent<DragAndDrop>().correcto)
        {
            item = null;
            spotifi.Resta();
        }
        else
        {
            if(item != null && item.transform.parent != transform)
            {
               item = null;
            }
        }
    }
}
