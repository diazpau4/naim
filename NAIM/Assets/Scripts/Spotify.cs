using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spotify : MonoBehaviour
{
    public int verificador = 0 ;
    public GameObject botonPlay, botonContinue;

    public void Suma()
    {
        verificador++;

        if (verificador >= 5)
        {
            botonPlay.SetActive(true);
            botonContinue.SetActive(true);
        }
    }

    public void Resta()
    {
        if (verificador == 0)
        {
            return;
        }
        else
        {
            verificador--;
            botonPlay.SetActive(false);
            botonContinue.SetActive(false);
        }

    }
    
}
