using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Screnemanager : MonoBehaviour
{
    public GameObject CNT;

    public void INICIO()
    {
        SceneManager.LoadScene("InicioXquien");
    }
    public void QUIENReloj()
    {
        SceneManager.LoadScene("QUIEN");
    }
    public void QUIEN()
    {
        SceneManager.LoadScene("QuienVID");
    }
    public void QuienVID()
    {
        SceneManager.LoadScene("QuienXCars");
    }

    public void CARSReloj()
    {
        SceneManager.LoadScene("CARS");
    }
    public void CARS()
    {
        SceneManager.LoadScene("CarsVID");
    }

    public void CARSVID()
    {
        SceneManager.LoadScene("CarsXSomeone");
    }

    public void SOMEONEReloj()
    {
        SceneManager.LoadScene("SOMEONE");
    }
    public void SOMEONE()
    {
        SceneManager.LoadScene("SomeoneVID");
    }

    public void SOMEONEVID()
    {
        SceneManager.LoadScene("SomeoneXMAI");
    }

    public void MAIReloj()
    {
        SceneManager.LoadScene("MAI");
    }

    public void MAI()
    {
        SceneManager.LoadScene("MAIVID");
    }
    public void MAIVID()
    {
        SceneManager.LoadScene("MAIXMR");
    }

    public void MRReloj()
    {
        SceneManager.LoadScene("Me rehuso");

    }
    public void MR()
    {
        SceneManager.LoadScene("MeRehuVID");

    }
    public void MRVID()
    {
        SceneManager.LoadScene("MeRehuXLaDosis");

    }
    public void LaDosisReloj()
    {
        SceneManager.LoadScene("La Dosis");
    }
    public void LaDosis()
    {
        SceneManager.LoadScene("LaDosisVID");
    }
    public void LaDosisVID()
    {
        SceneManager.LoadScene("Final");
    }

}
